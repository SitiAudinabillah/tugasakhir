-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2022 at 04:50 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `preta_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `hero_landing`
--

CREATE TABLE `hero_landing` (
  `id` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  `file_foto` varchar(128) NOT NULL,
  `status_persetujuan` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hero_landing`
--

INSERT INTO `hero_landing` (`id`, `label`, `description`, `file_foto`, `status_persetujuan`) VALUES
(1, 'Fashionable Dress', '10% Off Your First Order', 'default.jpg', 1),
(14, 'Reasonable Price', '10% Off Your First Order', 'carousel-6.jpg', 1),
(15, 'Finest Clothes', '10% Off Your First Order', 'carousel-4.jpg', 1),
(16, 'Stylist', 'Show', 'bag-1.jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `category` varchar(128) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `picture` varchar(128) NOT NULL,
  `approval_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category`, `price`, `description`, `picture`, `approval_status`) VALUES
(1, 'Brown Jacket Denim', 'Jacket', '299000.00', 'Cozy up in our Denim Regular Fit Jackets for a classic look. Pair with a slouchy tee and ripped jeans for an everyday look.', 'jacket-1.jpeg', 1),
(2, 'Jumpsuit Women O Neck', 'Suit', '399000.00', 'The style options for jumpsuits this season is seemingly endless. Since there are so many options available you can more than likely find one that suits your taste.', 'jumpsuit-1.jpeg', 1),
(3, 'Blazer outfit Men', 'Blazer ', '599000.00', 'Beige is one of the colors that harmonize with many men\'s styles. You can create combinations that you can use in almost any environment by using a beige blazer jacket, beige denim jacket, beige leather jacket or beige linen jacket.', 'blazer-1.jpeg', 1),
(4, 'Stylish Pants', 'Pant', '655000.00', 'Stylish and elegant pants for women. Embrace the season\'s new tailored silhouettes with our online collection of women\'s pant suits.', 'pants-2.jpeg', 1),
(5, 'Men\'s Ultra Cotton Longsleeve', 'Longsleeve', '99000.00', 'Long-sleeved are perfect for when it\'s getting chilly but not too cold outside.', 'longsleeve-1.jpeg', 0),
(6, 'Oversized Longsleeve', 'Longsleeve', '99000.00', 'oversized longsleeve selection for the very best in unique or custom, handmade pieces from our shops.', 'longsleeve-2.jpeg', 1),
(7, 'Cool Shirt', 'Shirt', '199000.00', 'A shirt is a piece of clothing that you wear on the upper part of your body. Shirts have a collar, sleeves, and buttons down the front', 'shirt-1.png', 1),
(8, 'Stylish Sling Bags', 'bag', '300000.00', 'Bags Are Made From High Quality Acrylic Fibers That Will Last A Lifetime.', 'bag-2.jpeg', 2),
(9, 'Straw Hat Women', 'hat', '75000.00', 'A traditional canvas safari hat will give you great protection from the sun, while a mesh safari hat has holes that might let some sun in but will give you great air flow to help keep you cool. For great all around protection, you\'ll want a classic style with a brim.', 'hat-1.jpeg', 1),
(10, 'Coat Women', 'coat', '855000.00', 'Coat is any of various types of outer clothing that are worn over other clothes, usually open in the front, and are often used for warmth', 'coat-1.jpeg', 1),
(12, 'Blue Sling Bag', 'Bag', '426000.00', 'sling stylist and latest Leather Sling Heavy chain handbag/shoulder bag for women', 'bag-3.jpeg', 1),
(14, 'Blazer Women', 'Blazer', '789000.00', 'While I don’t discriminate against any type of blazer, I would say a plaid blazer is my favorite for fall. There’s just nothing like an oversized, plaid blazer to make a look pop, and this one from Abercrombie is perfect.', 'blazer-5.jpeg', 1),
(15, 'Panama Hats', 'hat', '35000.00', 'Panama hats: leather, fur, flannel. So the usual summer accessories became winter and, finally, came to the masses. It should be added that panama suits most women and do not spoil their hair as much as hats.', 'hat-3.png', 1),
(16, 'Detective Coat', 'Coat', '999000.00', 'A trench coat is a specific kind of rain coat, which has a belt and is often double-breasted and buttons down the front. Movie spies and detectives are commonly shown wearing trench coats. The classic version of a trench coat is made from waterproof beige cotton, belted, and extends below the knee.', 'coat-5.jpeg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(5, 'Siti Audinabillah', 'audinabillah@gmail.com', 'default.png', '$2y$10$mGhd/LvLiFtlJsHrgNVRH.B2vljzTxRavu30Sap6gvwcWmWPjPvJS', 1, 1, 1664940674),
(6, 'audin', 'audin@mail.com', 'engineer.png', '$2y$10$/ombI1l4TlHqOssG/l17Q.nCKl.A3vOeNyCt5PTk7NJEu6hy069BC', 1, 1, 1669820838),
(7, 'aud', 'aud@mail.com', 'manager.png', '$2y$10$z64PZo.ENlVAhhL5VDsEjONrxhE42iTln6ARSZJOOQwAl5o/7/Nfu', 2, 1, 1670226891);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 2, 2),
(4, 1, 3),
(6, 2, 3),
(8, 2, 1),
(11, 2, 6),
(12, 1, 6),
(13, 2, 7),
(14, 1, 7),
(15, 3, 7),
(16, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'Manager'),
(3, 'Menu'),
(6, 'Hero'),
(7, 'Product');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'My Profile', 'admin', 'fas fa-fw  fa-user-astronaut', 1),
(2, 2, 'Approval Hero', 'manager/approval_hero', 'fas fa-fw fa-marker', 1),
(3, 1, 'Edit Profile', 'admin/edit_profile', 'fas fa-fw fa-user-edit', 1),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open ', 1),
(7, 2, 'Role', 'manager/role', 'fas fa-fw fa-user-tie', 1),
(8, 1, 'Change Password', 'admin/changepassword', 'fas fa-fw fa-key', 1),
(10, 6, 'Hero Landing Page', 'hero', 'fas fa-fw  fa-store', 1),
(11, 2, 'Approval Product', 'manager/approval_product', 'fas fa-fw  fa-pen', 1),
(12, 7, 'Product', 'product', 'fas fa-fw  fa-tag', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hero_landing`
--
ALTER TABLE `hero_landing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hero_landing`
--
ALTER TABLE `hero_landing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

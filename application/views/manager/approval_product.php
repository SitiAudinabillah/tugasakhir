<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="row">
        <!-- Page Heading -->
        <div class="col-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800"><?= $title; ?></h1>
                <div class="droppdown ml-4">
                    <button class="btn border dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sort by
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                        <a class="dropdown-item" href="<?= base_url("manager/approval_product") ?>">Show all</a>
                        <a class="dropdown-item" href="<?= base_url("manager/approval_product/0") ?>">Pending</a>
                        <a class="dropdown-item" href="<?= base_url("manager/approval_product/1") ?>">Approved</a>
                        <a class="dropdown-item" href="<?= base_url("manager/approval_product/2") ?>">Rejected</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Page Heading -->

        <div class="col-lg">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">

                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Description</th>
                        <th scope="col">Picture</th>
                        <th scope="col">Approval Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody style="background-color: #fbeeee;">
                    <?php $i = 1 ?>
                    <?php foreach ($approval_product as $p) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $p['name']; ?></td>
                            <td><?= $p['category']; ?></td>
                            <td><?= $p['price']; ?></td>
                            <td><?= $p['description']; ?></td>
                            <td>
                                <img src="<?= base_url('assets/img/product/') . $p['picture'];  ?>" name="picture" class="img-thumbnail" width="150px">
                                <p><?= $p['picture']; ?></p>
                            </td>
                            <td><?php if ($p['approval_status'] == 0) {
                                    echo "Pending";
                                } elseif ($p['approval_status'] == 1) {
                                    echo "Approved";
                                } else {
                                    echo "Rejected";
                                } ?></td>
                            <td>
                                <a href="" data-toggle="modal" data-target="#editProductModal<?= $p['id'] ?>" class="badge badge-primary"><i class="far fa-fw fa-edit"></i></a>
                                <a href="<?= base_url('manager/deleteProduct/' . $p['id']) ?>" class="badge badge-danger" onclick="return confirm('Are you sure want to delete <?= $p['name']; ?> ?')"><i class="far fa-fw fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>

            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Edit Modal -->
<?php foreach ($approval_product as $pt) : ?>
    <div class="modal fade" id="editProductModal<?= $pt['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editProductModal<?= $pt['id'] ?>Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editProductModal<?= $pt['id'] ?>Label">Edit Product</h5>
                    <buttond type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </buttond>
                </div>

                <form action="<?= base_url('manager/editProduct/' . $pt['id']); ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $pt['name'] ?>" id="name" name="name" placeholder="Product name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $pt['category'] ?>" id="category" name="category" placeholder="Product category">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $pt['price'] ?>" id="price" name="price" placeholder="Product price">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="description" name="description" placeholder="Product description"><?= $pt['description'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="picture" name="picture" value="<?= $pt['picture'] ?>">
                                <label class="custom-file-label" for="picture">Choose file photo</label>
                            </div>
                        </div>
                        <div class="form-group"><label for="approval_status<?= $pt['id'] ?>">Status</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="approval_status" id="approval_status1<?= $pt['id'] ?>" value="0" <?= $pt['approval_status'] == 0 ? "checked" : ""; ?>>
                                <label class="form-check-label" for="approval_status1<?= $pt['id'] ?>">
                                    Pending
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="approval_status" id="approval_status2<?= $pt['id'] ?>" value="1" <?= $pt['approval_status'] == 1 ? "checked" : ""; ?>>
                                <label class="form-check-label" for="approval_status2<?= $pt['id'] ?>">
                                    Approved
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="approval_status" id="approval_status3<?= $pt['id'] ?>" value="2" <?= $pt['approval_status'] == 2 ? "checked" : ""; ?>>
                                <label class="form-check-label" for="approval_status3<?= $pt['id'] ?>">
                                    Rejected
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<!-- End Edit Modal -->
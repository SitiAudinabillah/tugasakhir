<?php $currentPage = isset($currentPage) ? $currentPage : " "  ?>
<!-- Navbar Start -->
<div class="container-fluid">
    <div class="row align-items-center py-3 px-xl-5">
        <div class="col-lg-3 d-none d-lg-block">
            <a href="" class="text-decoration-none">
                <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">PRETA</span></h1>
            </a>
        </div>
        <div class="col-lg-6 col-6 text-left">
            <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0">
                <a href="" class="text-decoration-none d-block d-lg-none">
                    <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">PRETA</span></h1>
                </a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav py-0 mx-auto">
                        <a href="<?= base_url('landing_page'); ?>" class="nav-item nav-link <?= $currentPage == "home" ? "active" : "" ?>">Home</a>
                        <a href="<?= base_url('shop'); ?>" class="nav-item nav-link <?= $currentPage == "shop" ? "active" : "" ?>">Shop</a>
                        <a href="<?= base_url('landing_page/kontak'); ?>" class="nav-item nav-link <?= $currentPage == "kontak" ? "active" : "" ?>">Contact</a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-lg-3 col-6 text-right">
            <a href="" class="btn border" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Product you've likes">
                <i class="fas fa-heart text-primary"></i>
                <span class="badge">0</span>
            </a>
            <a href="" class="btn border" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Your shopping cart">
                <i class="fas fa-shopping-cart text-primary"></i>
                <span class="badge">0</span>
            </a>
            <a href="<?= base_url('auth'); ?>" class="btn border" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Sign in">
                <i class="fas fa-user text-primary"></i>
            </a>
        </div>
    </div>
</div>
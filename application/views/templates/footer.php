<!-- Back to Top -->
<a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- Footer Start -->
<div class="container-fluid bg-secondary text-dark mt-5 pt-5">
    <div class="row px-xl-5 pt-5">
        <div class="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5">
            <a href="" class="text-decoration-none">
                <h1 class="mb-4 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border border-white px-3 mr-1">PRETA</span></h1>
            </a>
            <p>Preta fashion companies is to offer fashion and quality at the best price in a sustainable way.</p>
            <p class="mb-2"><i class="fa fa-map-marker-alt text-primary mr-3"></i>Baker Street, London, England</p>
            <p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>preta.info@gmail.com</p>
            <p class="mb-0"><i class="fa fa-phone-alt text-primary mr-3"></i>+44 20 7389 3680</p>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="row">
                <div class="col-md-4 mb-4">
                    <h5 class="font-weight-bold text-dark mb-4">Quick Links</h5>
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-dark mb-2" href="<?= base_url('landing_page'); ?>"><i class="fa fa-angle-right mr-2"></i>Home</a>
                        <a class="text-dark mb-2" href="<?= base_url('shop'); ?>"><i class="fa fa-angle-right mr-2"></i>Our Shop</a>
                        <a class="text-dark mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Categories</a>
                        <a class="text-dark mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Shopping Cart</a>
                        <a class="text-dark" href="#"><i class="fa fa-angle-right mr-2"></i>Checkout</a>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <h5 class="font-weight-bold text-dark mb-4">Quick Links</h5>
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-dark mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>FAQs</a>
                        <a class="text-dark mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Help</a>
                        <a class="text-dark mb-2" href="<?= base_url('landing_page/kontak'); ?>"><i class="fa fa-angle-right mr-2"></i>Contact Us</a>
                        <a class="text-dark mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Company</a>
                        <a class="text-dark" href="#"><i class="fa fa-angle-right mr-2"></i>Support</a>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <h5 class="font-weight-bold text-dark mb-4">Newsletter</h5>
                    <form action="">
                        <div class="form-group">
                            <input type="text" class="form-control border-0 py-4" placeholder="Your Name" required="required" />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control border-0 py-4" placeholder="Your Email" required="required" />
                        </div>
                        <div>
                            <button class="btn btn-primary btn-block border-0 py-3" type="submit">Subscribe Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row py-2 px-xl-5 border-bottom border-light">
        <div class="col-lg-6 d-none d-lg-block">
        </div>
        <div class="col-lg-6 text-center text-lg-right">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark px-2" href="">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-twitter"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-linkedin-in"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-instagram"></i>
                </a>
                <a class="text-dark pl-2" href="">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row mx-xl-5 py-4">
        <div class="col-md-6 px-xl-0">
            <p class="mb-md-0 text-center text-md-left text-dark">
                &copy; <a class="text-dark font-weight-semi-bold" href="#">PRETA</a>. All Rights Reserved.
            </p>
        </div>
        <div class="col-md-6 px-xl-0">
            <p class="mb-md-0 text-center text-md-right text-dark">
                <img class="img-fluid" src="assets/img/payments.png" alt="">
            </p>
        </div>
    </div>
    <!-- Footer End -->

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/lib/easing/easing.min.js"></script>
    <script src="<?= base_url(); ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?= base_url(); ?>assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?= base_url(); ?>assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?= base_url(); ?>assets/js/main.js"></script>

    <script>
        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    </body>

    </html>
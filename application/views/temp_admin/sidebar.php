 <!-- Sidebar -->
 <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #D19C97;">

     <!-- Sidebar - Brand -->
     <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url("landing_page") ?>">
         <h1 class="sidebar-brand-text display-5 m-0 font-weight-semi-bold"><span class="text-light font-weight-bold border px-1 mr-1">PRETA</span></h1>
     </a>

     <!-- Divider -->
     <hr class="sidebar-divider">

     <!-- Query menu -->
     <?php
        $role_id = $this->session->userdata('role_id');
        $queryMenu = "SELECT `user_menu`.`id`,`menu`
                        FROM `user_menu` JOIN `user_access_menu` 
                        ON `user_menu`.`id` = `user_access_menu`.`menu_id`
                        WHERE `user_access_menu`.`role_id` = $role_id
                        ORDER BY `user_access_menu`.`menu_id` ASC
                    ";
        $menu = $this->db->query($queryMenu)->result_array();
        ?>

     <!-- looping menu -->
     <?php foreach ($menu as $m) :  ?>
         <div class="sidebar-heading">
             <?= $m['menu']; ?>
         </div>
         <!-- sub menu -->

         <?php
            $menuId = $m['id'];
            $querySubMenu =  "SELECT * FROM `user_sub_menu` 
                            WHERE `menu_id` = $menuId
                            AND `is_active` = 1
                            ";
            $subMenu = $this->db->query($querySubMenu)->result_array();
            ?>

         <?php foreach ($subMenu as $sm) : ?>
             <?php if ($title == $sm['title']) : ?>
                 <li class="nav-item active">
                 <?php else : ?>
                 <li class="nav-item">
                 <?php endif; ?>

                 <a class="nav-link pb-0" href="<?= base_url($sm['url']); ?>">
                     <i class="<?= $sm['icon']; ?>"></i>
                     <span><?= $sm['title']; ?></span></a>
                 </li>

             <?php endforeach; ?>

             <hr class="sidebar-divider mt-3">

         <?php endforeach; ?>

         <!-- Nav Item - logout -->
         <li class="nav-item">
             <a class="nav-link" href="<?= base_url('auth/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                 <i class="fas fa-fw fa-sign-out-alt"></i>
                 <span>Logout</span></a>
         </li>

         <!-- Divider -->
         <hr class="sidebar-divider d-none d-md-block">

         <!-- Sidebar Toggler (Sidebar) -->
         <div class="text-center d-none d-md-inline">
             <button class="rounded-circle border-0" id="sidebarToggle"></button>
         </div>

 </ul>
 <!-- End of Sidebar -->
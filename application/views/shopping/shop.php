<!-- Page Header Start -->
<div class="container-fluid bg-secondary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 180px">
        <h1 class="font-weight-semi-bold text-uppercase mb-3">PRETA STORE</h1>
    </div>
</div>
<!-- Page Header End -->
<!-- Shop Start -->
<div class="container-fluid pt-5">
    <div class="row px-xl-5">
        <!-- Shop Sidebar Start -->
        <div class="col-lg-3 col-md-12">

            <!-- Categories Start -->
            <div class="border-bottom mb-4 pb-4">
                <h5 class="font-weight-semi-bold mb-4">Filter by categories</h5>
                <form>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-0" value="0" <?= $filterCategory == '0' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-0">All Categories</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-1" value="1" <?= $filterCategory == '1' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-1">Shirts</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-2" value="2" <?= $filterCategory == '2' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-2">Hats</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-3" value="3" <?= $filterCategory == '3' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-3">Suits</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-4" value="4" <?= $filterCategory == '4' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-4">Blazers</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-5" value="5" <?= $filterCategory == '5' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-5">Jackets</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-6" value="6" <?= $filterCategory == '6' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-6">Coats</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-7" value="7" <?= $filterCategory == '7' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-7">Bags</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-8" value="8" <?= $filterCategory == '8' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-8">Longsleeves</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between">
                        <input type="radio" class="form-check-input filter-cat" name="fCategories" id="fCategories-9" value="9" <?= $filterCategory == '9' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="fCategories-9">Pants</label>
                    </div>
                </form>
            </div>
            <!-- Categories End -->

            <!-- Price Start -->
            <div class="border-bottom mb-4 pb-4">
                <h5 class="font-weight-semi-bold mb-4">Filter by price</h5>
                <form>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-0" value="0" <?= $filterPrice == '0' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-0">All Price</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-1" value="1" <?= $filterPrice == '1' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-1">Rp 0 - Rp 100.000</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-2" value="2" <?= $filterPrice == '2' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-2">Rp 100.000 - Rp 200.000</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-3" value="3" <?= $filterPrice == '3' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-3">Rp 200.000 - Rp 300.000</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-4" value="4" <?= $filterPrice == '4' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-4">Rp 300.000 - Rp 400.000</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-5" value="5" <?= $filterPrice == '5' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-5">Rp 400.000 - Rp 500.000</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-6" value="6" <?= $filterPrice == '6' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-6">Rp 500.000 - Rp 700.000</label>
                    </div>
                    <div class="form-check d-flex align-items-center justify-content-between">
                        <input type="radio" class="form-check-input filter-price" name="fPrice" id="price-7" value="7" <?= $filterPrice == '7' ? "checked" : ""; ?>>
                        <label class="form-check-label" for="price-7">Rp 700.000 - Rp 1.000.000</label>
                    </div>
                    <script>
                        <?php
                        $s = isset($search) ? $search : "";
                        $p = isset($filterPrice) ? $filterPrice : "";
                        $c = isset($filterCategory) ? $filterCategory : "";
                        ?>
                        let par = {
                            search: "<?= $s ?>",
                            category: "<?= $c ?>",
                            price: "<?= $p ?>"
                        };

                        function buildParams(o) {
                            let url = `<?= base_url('shop/index/') ?>?search=${o.search}&price=${o.price}&cat=${o.category}#src`;
                            window.location.href = url;
                            console.log(url);
                        }

                        let fCat = document.getElementsByClassName("filter-cat");
                        for (const i of fCat) {
                            i.addEventListener("click", function name(params) {
                                par.category = i.value
                                buildParams(par);
                            })
                        }

                        let fPrice = document.getElementsByClassName("filter-price");
                        for (const i of fPrice) {
                            i.addEventListener("click", function name(params) {
                                par.price = i.value
                                buildParams(par);
                            })
                        }
                    </script>
                </form>
            </div>
            <!-- Price End -->

        </div>
        <!-- Shop Sidebar End -->

        <!-- Shop Product Start -->
        <div class="col-lg-9 col-md-12">
            <div class="row pb-3">
                <div class="col-12 pb-1">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <form action="<?= base_url('shop/index/#src') ?>" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control" placeholder="Search for products" value="<?= isset($search) ? $search : ""; ?>">
                                <input type="hidden" name="price" value="<?= $p; ?>">
                                <input type="hidden" name="cat" value="<?= $p; ?>">
                                <div class="input-group-append">
                                    <button class="input-group-text bg-transparent text-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                    <button class="input-group-text bg-transparent text-primary" type="submit">
                                        <a href="<?= base_url('shop/index') ?>">reset</a>
                                    </button>
                                </div>
                            </div>
                            <?= $this->session->flashdata('message');  ?>
                        </form>
                    </div>
                </div>
                <div id="src"></div>
                <?php foreach ($items as $is) : ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 pb-1" style="max-width: 300px;">
                        <div class="card product-item border-0 mb-4">
                            <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                                <img class="img-fluid w-100" src="<?= base_url('assets/img/product/') . $is['picture']; ?>" alt="pic" style=" height: 390px; background-size:cover;">
                            </div>
                            <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                                <h6 class="text-truncate mb-2"><?= $is['name']; ?></h6>
                                <p class="text-truncate mb-3" style="font-size: 12px;">Category: <?= $is['category']; ?></p>
                                <div class="d-flex justify-content-center">
                                    <h6><?= $is['price']; ?></h6>
                                    <!-- <h6 class="text-muted ml-2"><del>$123.00</del></h6> -->
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-between bg-light border">
                                <a href="<?= base_url('shop/details/') . $is['id']; ?>" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                                <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <div class="col-12 pb-1">
                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-center mb-3">
                            <?php //$q = isset($search) ? "?search=" . $search : "";
                            $url = "?search=$s&price=$p&cat=$c#src" ?>
                            <li class="page-item <?= $pageNum == '1' ? "disabled" : ""; ?>">
                                <a class="page-link" href="<?= base_url("shop/index/" . ($pageNum - 1) . "$url"); ?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php for ($i = 1; $i <= $maxPage; $i++) : ?>
                                <li class="page-item"><a class="page-link" href="<?= base_url("shop/index/$i" . $url); ?>"><?= $i; ?></a></li>
                            <?php endfor; ?>

                            <li class="page-item <?= $pageNum == "$maxPage" ? "disabled" : ""; ?>">
                                <a class="page-link" href="<?= base_url("shop/index/" . ($pageNum + 1) . $url); ?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Shop Product End -->
    </div>
</div>
<!-- Shop End -->
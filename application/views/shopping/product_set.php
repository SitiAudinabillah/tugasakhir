<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-info mb-3" data-toggle="modal" data-target="#newProductAddModal">Add New Product</a>
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Description</th>
                        <th scope="col">Picture</th>
                        <th scope="col">Approval Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody style="background-color: #fbeeee;">
                    <?php $i = 1 ?>
                    <?php foreach ($product as $p) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $p['name']; ?></td>
                            <td><?= $p['category']; ?></td>
                            <td><?= $p['price']; ?></td>
                            <td><?= $p['description']; ?></td>
                            <td>
                                <img src="<?= base_url('assets/img/product/') . $p['picture'];  ?>" name="picture" class="img-thumbnail" width="150px">
                                <p><?= $p['picture']; ?></p>
                            </td>
                            <td><?php if ($p['approval_status'] == 0) {
                                    echo "Pending";
                                } elseif ($p['approval_status'] == 1) {
                                    echo "Approved";
                                } else {
                                    echo "Rejected";
                                } ?></td>
                            <td>
                                <a href="" data-toggle="modal" data-target="#editProductModal<?= $p['id'] ?>" class="badge badge-primary"><i class="far fa-fw fa-edit"></i></a>
                                <a href="<?= base_url('product/deleteProduct/' . $p['id']) ?>" class="badge badge-danger" onclick="return confirm('Are you sure want to delete <?= $p['name']; ?> ?')"><i class="far fa-fw fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newProductAddModal" tabindex="-1" role="dialog" aria-labelledby="newProductAddModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newProductAddModalLabel">Add New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="<?= base_url('product'); ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Product name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="category" name="category" placeholder="Product category">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="price" name="price" placeholder="Product price">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="description" name="description" placeholder="Product description"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="picture" name="picture">
                            <label class="custom-file-label" for="picture">Choose file photo</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal -->

<!-- Edit Modal -->
<?php foreach ($product as $pt) : ?>
    <div class="modal fade" id="editProductModal<?= $pt['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editProductModal<?= $pt['id'] ?>Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editProductModal<?= $pt['id'] ?>Label">Edit Product</h5>
                    <buttond type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </buttond>
                </div>

                <form action="<?= base_url('product/editProduct/' . $pt['id']); ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $pt['name'] ?>" id="name" name="name" placeholder="Product name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $pt['category'] ?>" id="category" name="category" placeholder="Product category">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $pt['price'] ?>" id="price" name="price" placeholder="Product price">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="description" name="description" placeholder="Product description"><?= $pt['description'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="picture" name="picture" value="<?= $pt['picture'] ?>">
                                <label class="custom-file-label" for="picture">Choose file photo</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<!-- End Edit Modal -->
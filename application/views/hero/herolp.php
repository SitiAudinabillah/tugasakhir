<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-info mb-3" data-toggle="modal" data-target="#newHerolpModal">Add New Hero</a>
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Label</th>
                        <th scope="col">Description</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Status Persetujuan</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 ?>
                    <?php foreach ($hero as $h) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $h['label']; ?></td>
                            <td><?= $h['description']; ?></td>
                            <td>
                                <img src="<?= base_url('assets/img/hero/') . $h['file_foto'];  ?>" name="file_foto" class="img-thumbnail" width="150px">
                                <p><?= $h['file_foto']; ?></p>
                            </td>
                            <td><?php if ($h['status_persetujuan'] == 0) {
                                    echo "Pending";
                                } elseif ($h['status_persetujuan'] == 1) {
                                    echo "Approved";
                                } else {
                                    echo "Rejected";
                                } ?></td>
                            <td>
                                <a href="" data-toggle="modal" data-target="#editHerolpModal<?= $h['id'] ?>" class="badge badge-primary"><i class="far fa-fw fa-edit"></i></a>
                                <a href="<?= base_url('hero/deleteHerolp/' . $h['id']) ?>" class="badge badge-danger" onclick="return confirm('Are you sure want to delete <?= $h['label']; ?> ?')"><i class="far fa-fw fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newHerolpModal" tabindex="-1" role="dialog" aria-labelledby="newHerolpModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newHerolpModalLabel">Add New Hero</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="<?= base_url('hero'); ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="label" name="label" placeholder="Hero label">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="description" name="description" placeholder="Hero description">
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file_foto" name="file_foto">
                            <label class="custom-file-label" for="file_foto">Choose file photo</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal -->

<!-- Edit Modal -->
<?php foreach ($hero as $hlp) : ?>
    <div class="modal fade" id="editHerolpModal<?= $hlp['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editHerolpModal<?= $hlp['id'] ?>Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editHerolpModal<?= $hlp['id'] ?>Label">Edit Hero</h5>
                    <buttond type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </buttond>
                </div>

                <form action="<?= base_url('hero/editHerolp/' . $hlp['id']); ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $hlp['label'] ?>" id="label" name="label" placeholder="Hero label">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $hlp['description'] ?>" id="description" name="description" placeholder="Hero description">
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="file_foto" name="file_foto" value="<?= $hlp['file_foto'] ?>">
                                <label class="custom-file-label" for="file_foto">Choose file photo</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<!-- End Edit Modal -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hero_model extends CI_Model
{
    public function getHerolp()
    {
        $query = "SELECT * FROM `hero_landing`";
        return $this->db->query($query)->result_array();
    }

    public function addHerolp()
    {
        $statusPersetujuan = $this->input->post('status_persetujuan');
        if (!isset($statusPersetujuan)) {
            $statusPersetujuan = 0;
        }

        $data = array(
            'label' => $this->input->post('label'),
            'description' => $this->input->post('description'),
            'file_foto' => "default.jpg",
            'status_persetujuan' => $statusPersetujuan
        );

        $upload_image = $_FILES['file_foto']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = '2048';
            $config['upload_path']   = './assets/img/hero/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file_foto')) {
                $res = array('upload_data' => $this->upload->data());
                $data['file_foto'] = $res['upload_data']['file_name'];
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                redirect('hero');
            }
        }
        $this->db->insert('hero_landing', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Hero successfully added!</div>');
        redirect('hero');
    }

    public function saveHerolp($id)
    {
        $data = array(
            'label' => $this->input->post('label'),
            'description' => $this->input->post('description'),
            // 'file_foto' => $this->input->post('file_foto'),
            'status_persetujuan' => $this->input->post('status_persetujuan')
        );

        $upload_image = $_FILES['file_foto']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = '2048';
            $config['upload_path']   = './assets/img/hero/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file_foto')) {
                $this->deleteImage($id);
                $new_image = $this->upload->data('file_name');
                $data['file_foto'] = $new_image;
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                redirect('hero');
            }
        }
        $this->db->where('id', $id);
        $this->db->update('hero_landing', $data);
    }

    public function deleteImage($id)
    {
        $oldData = $this->db->query("SELECT * FROM `hero_landing` WHERE id=$id")->result_array()[0];
        $old_image = $oldData['file_foto'];
        try {
            if ($old_image != 'default.jpg') {
                $path = FCPATH . 'assets/img/hero/' . $old_image;
                unlink($path);
            }
        } catch (\Throwable $th) {
            echo "Something went wrong";
        }
    }

    public function deleteHerolp($id)
    {
        $this->deleteImage($id);
        $this->db->delete('hero_landing', ['id' => $id]);
    }

    public function getHeroByStatus()
    {
        $this->db->select("*");
        $this->db->from("hero_landing");
        $this->db->where("status_persetujuan", "1");
        $get = $this->db->get();
        return $get->result_array();
    }

    public function getHeroByFilter($filter)
    {
        $this->db->select("*");
        $this->db->from("hero_landing");
        $this->db->where("status_persetujuan", $filter);
        $get = $this->db->get();
        return $get->result_array();
    }
}

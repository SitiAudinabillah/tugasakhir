<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
    public function getProduct()
    {
        $query = "SELECT * FROM `product`";
        return $this->db->query($query)->result_array();
    }

    public function addProduct()
    {
        $approvalStatus = $this->input->post('approval_status');
        if (!isset($approvalStatus)) {
            $approvalStatus = 0;
        }

        $data = array(
            'name' => $this->input->post('name'),
            'category' => $this->input->post('category'),
            'price' => $this->input->post('price'),
            'description' => $this->input->post('description'),
            'picture' => "default.jpg",
            'approval_status' => $approvalStatus
        );

        $upload_image = $_FILES['picture']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = '2048';
            $config['upload_path']   = './assets/img/product/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('picture')) {
                $res = array('upload_data' => $this->upload->data());
                $data['picture'] = $res['upload_data']['file_name'];
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                redirect('product');
            }
        }
        $this->db->insert('product', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Product successfully added!</div>');
        redirect('product');
    }

    public function saveProduct($id)
    {
        $data = array(
            'name' => $this->input->post('name'),
            'category' => $this->input->post('category'),
            'price' => $this->input->post('price'),
            'description' => $this->input->post('description'),
            // 'picture' => $this->input->post('picture'),
            'approval_status' => $this->input->post('approval_status')
        );

        $upload_image = $_FILES['picture']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = '2048';
            $config['upload_path']   = './assets/img/product/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('picture')) {
                $this->deleteImage($id);
                $new_image = $this->upload->data('file_name');
                $data['picture'] = $new_image;
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                redirect('product');
            }
        }
        $this->db->where('id', $id);
        $this->db->update('product', $data);
    }

    public function deleteImage($id)
    {
        $oldData = $this->db->query("SELECT * FROM `product` WHERE id=$id")->result_array()[0];
        $old_image = $oldData['picture'];
        try {
            if ($old_image != 'default.jpg') {
                $path = FCPATH . 'assets/img/product/' . $old_image;
                unlink($path);
            }
        } catch (\Throwable $th) {
            echo "Something went wrong";
        }
    }

    public function deleteProduct($id)
    {
        $this->deleteImage($id);
        $this->db->delete('product', ['id' => $id]);
    }

    public function getProductByStatus($status, $page = 0, $pageSize = 2)
    {
        $offset = ($page - 1) * $pageSize;
        $this->db->select("*");
        $this->db->from("product");
        $this->db->where("approval_status", $status);
        if ($page != 0) {
            $this->db->limit($pageSize, $offset);
        }
        $get = $this->db->get();
        return $get->result_array();
    }

    public function getProductByFilter($filter)
    {
        $this->db->select("*");
        $this->db->from("product");
        $this->db->where("approval_status", $filter);
        $get = $this->db->get();
        return $get->result_array();
    }

    public function getProductById($id)
    {
        $this->db->select("*");
        $this->db->from("product");
        $this->db->where("id", $id);
        $this->db->where("approval_status", '1');
        $get = $this->db->get();
        $res = $get->result_array();
        return ($res) ? $res[0] : [];
    }

    public function searchFilter($search, $page = 0, $pageSize = 2, $fCategory = 0, $fPrice = 0)
    {
        $page = $page <= 1 ? 1 : $page;
        $offset = ($page - 1) * $pageSize;
        $p = $this->getCategory($fCategory);
        $c = $this->getPriceRange($fPrice);
        $query = "SELECT *
        FROM `product`
        WHERE (approval_status='1' $p $c) AND name LIKE '%$search%' 
                OR
              (approval_status='1' $p $c) AND category LIKE '%$search%'
            LIMIT $offset, $pageSize;";

        $res = $this->db->query($query)->result_array();

        if ($res) {
            return $res;
        } else {
            return [];
        }
    }

    private function getCategory($c)
    {
        $cat = "";
        switch ($c) {
            case 0:
                $cat = "";
                break;
            case 1:
                $cat = "AND category = 'Shirt'";
                break;
            case 2:
                $cat = "AND category = 'Hat'";
                break;
            case 3:
                $cat = "AND category = 'Suit'";
                break;
            case 4:
                $cat = "AND category = 'Blazer'";
                break;
            case 5:
                $cat = "AND category = 'Jacket'";
                break;
            case 6:
                $cat = "AND category = 'Coat'";
                break;
            case 7:
                $cat = "AND category = 'Bag'";
                break;
            case 8:
                $cat = "AND category = 'Longsleeve'";
                break;
            case 9:
                $cat = "AND category = 'Pant'";
                break;
            default:
                $cat = "";
                break;
        }
        return $cat;
    }
    private function getPriceRange($priceCode)
    {
        $p = "";
        switch ($priceCode) {
            case 0:
                $p = "";
                break;
            case 1:
                $p = "AND price BETWEEN 0 AND 100000";
                break;
            case 2:
                $p = "AND price BETWEEN 100000 AND 200000";
                break;
            case 3:
                $p = "AND price BETWEEN 200000 AND 300000";
                break;
            case 4:
                $p = "AND price BETWEEN 300000 AND 400000";
                break;
            case 5:
                $p = "AND price BETWEEN 400000 AND 500000";
                break;
            case 6:
                $p = "AND price BETWEEN 500000 AND 700000";
                break;
            case 7:
                $p = "AND price BETWEEN 700000 AND 1000000";
                break;
            default:
                $p = "";
                break;
        }
        return $p;
    }
}

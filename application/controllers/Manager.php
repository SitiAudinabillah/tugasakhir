<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manager extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Hero_model', 'hero');
        $this->load->model('Product_model', 'product');
    }

    public function index()
    {
        $data['title'] = 'My Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('temp_admin/header', $data);
        $this->load->view('temp_admin/sidebar', $data);
        $this->load->view('temp_admin/topbar', $data);
        $this->load->view('manager/profile', $data);
        $this->load->view('temp_admin/footer');
    }

    // APPROVAL HERO
    public function approval_hero($filter = "")
    {
        $data['title'] = 'Approval Hero';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Hero_model', 'hero');
        if ($filter == "") {
            $data['approval_hero'] = $this->db->get('hero_landing')->result_array();
        } else {
            $data['approval_hero'] = $this->hero->getHeroByFilter($filter);
        }

        $this->load->view('temp_admin/header', $data);
        $this->load->view('temp_admin/sidebar', $data);
        $this->load->view('temp_admin/topbar', $data);
        $this->load->view('manager/approval_hero', $data);
        $this->load->view('temp_admin/footer');
    }

    public function editHerolp($id)
    {
        $this->hero->saveHerolp($id);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Hero has been edited!</div>');
        redirect('manager/approval_hero');
    }

    public function deleteHerolp($id)
    {
        $this->hero->deleteHerolp($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Hero has been deleted!</div>');
        redirect('manager/approval_hero');
    }
    //END APPROVAL HERO

    //APPROVAL PRODUCT
    public function approval_product($filter = "")
    {
        $data['title'] = 'Approval Product';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Product_model', 'product');
        if ($filter == "") {
            $data['approval_product'] = $this->db->get('product')->result_array();
        } else {
            $data['approval_product'] = $this->product->getProductByFilter($filter);
        }

        $this->load->view('temp_admin/header', $data);
        $this->load->view('temp_admin/sidebar', $data);
        $this->load->view('temp_admin/topbar', $data);
        $this->load->view('manager/approval_product', $data);
        $this->load->view('temp_admin/footer');
    }

    public function editProduct($id)
    {
        $this->product->saveProduct($id);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Product has been edited!</div>');
        redirect('manager/approval_product');
    }

    public function deleteProduct($id)
    {
        $this->product->deleteProduct($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Product has been deleted!</div>');
        redirect('manager/approval_product');
    }
    //END APPROVAL PRODUCT

    //ROLE
    public function role()
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get('user_role')->result_array();
        $this->form_validation->set_rules('role', 'Role', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('temp_admin/header', $data);
            $this->load->view('temp_admin/sidebar', $data);
            $this->load->view('temp_admin/topbar', $data);
            $this->load->view('manager/role', $data);
            $this->load->view('temp_admin/footer');
        } else {
            $this->db->insert('user_role', ['role' => $this->input->post('role')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New role added!</div>');
            redirect('manager/role');
        }
    }

    public function editRole($id)
    {
        $this->db->update('user_role', ['role' => $this->input->post('role')], ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Role has been edited!</div>');
        redirect('manager/role');
    }

    public function deleteRole($id)
    {
        $this->db->delete('user_role', ['id' => $id]);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Role has been deleted!</div>');
        redirect('manager/role');
    }
    //END ROLE

    public function roleAccess($role_id)
    {
        $data['title'] = 'Role Access';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        $this->db->where('id !=', 2);

        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->view('temp_admin/header', $data);
        $this->load->view('temp_admin/sidebar', $data);
        $this->load->view('temp_admin/topbar', $data);
        $this->load->view('manager/role-access', $data);
        $this->load->view('temp_admin/footer');
    }


    public function changeaccess()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];

        $result = $this->db->get_where('user_access_menu', $data);

        if ($result->num_rows() < 1) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Changed!</div>');
    }
}

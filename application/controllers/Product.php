<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Product_model', 'product');
    }

    public function index()
    {
        $data['title'] = 'Product Items';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Product_model', 'product');
        $data['product'] = $this->db->get('product')->result_array();

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('temp_admin/header', $data);
            $this->load->view('temp_admin/sidebar', $data);
            $this->load->view('temp_admin/topbar', $data);
            $this->load->view('shopping/product_set', $data);
            $this->load->view('temp_admin/footer');
        } else {
            $this->product->addProduct();
        }
    }

    public function editProduct($id)
    {
        $this->product->saveProduct($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Product has been edited!</div>');
        redirect('product');
    }

    public function deleteProduct($id)
    {
        $this->product->deleteProduct($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Product has been deleted!</div>');
        redirect('product');
    }



    public function rupiah($price)
    {
        $hasil_rupiah = "Rp " . number_format($price, 2, ',', '.');
        return $hasil_rupiah;
    }
}

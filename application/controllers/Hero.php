<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hero extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Hero_model', 'hero');
    }

    public function index()
    {
        $data['title'] = 'Hero Landing Page';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Hero_model', 'hero');
        $data['hero'] = $this->db->get('hero_landing')->result_array();

        $this->form_validation->set_rules('label', 'Label', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('temp_admin/header', $data);
            $this->load->view('temp_admin/sidebar', $data);
            $this->load->view('temp_admin/topbar', $data);
            $this->load->view('hero/herolp', $data);
            $this->load->view('temp_admin/footer');
        } else {
            $this->hero->addHerolp();
        }
    }

    public function editHerolp($id)
    {
        $this->hero->saveHerolp($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Hero has been edited!</div>');
        redirect('hero');
    }

    public function deleteHerolp($id)
    {
        $this->hero->deleteHerolp($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Hero has been deleted!</div>');
        redirect('hero');
    }
}

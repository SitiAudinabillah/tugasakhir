<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing_page extends CI_Controller
{
    public function index()
    {
        $data['currentPage'] = 'home';
        $this->load->view('templates/head');
        $this->load->view('templates/navbar', $data);

        $this->load->model('Hero_model');
        $data['foto'] = $this->Hero_model->getHeroByStatus();
        $this->load->view('hero/hero', $data);

        $this->load->view('landing_page/landing_page');
        $this->load->view('landing_page/offer');

        $this->load->model('Product_model');
        $data['items'] = $this->Product_model->getProductByStatus('1', '1', '8');


        foreach ($data['items'] as &$item) {
            $item['price'] = $this->rupiah($item['price']);
        }

        $this->load->view('shopping/product', $data);

        $this->load->view('templates/footer');
    }

    public function kontak()
    {
        $data['currentPage'] = 'kontak';
        $this->load->view('templates/head');
        $this->load->view('templates/navbar', $data);
        $this->load->view('landing_page/contact');
        $this->load->view('templates/footer');
    }

    public function rupiah($price)
    {
        $hasil_rupiah = "Rp " . number_format($price, 2, ',', '.');
        return $hasil_rupiah;
    }
}

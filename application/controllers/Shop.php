<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shop extends CI_Controller
{
    public function index($page = 1)
    {
        if (!ctype_digit("$page")) {
            $page = 1;
        }
        $page = $page <= 0 ? 1 : $page;
        $data['currentPage'] = 'shop';
        $data['pageNum'] = intval($page);
        $this->load->view('templates/head');
        $this->load->view('templates/navbar', $data);

        $search = !isset($_GET['search']) ? "" : $_GET['search'];
        $data['filterPrice'] = $fPrice = !isset($_GET['price']) ? "" : $_GET['price'];
        $data['filterCategory'] = $fCategory = !isset($_GET['cat']) ? "" : $_GET['cat'];

        $this->load->model('Product_model', 'product');
        $pageSize = 1;
        if ($search == "" && ($fPrice == "" || $fPrice == "0") && ($fCategory == "" || $fCategory == '0')) {
            $data['items'] = $this->product->getProductByStatus("1");
            $data['maxPage'] = ceil(sizeof($this->product->getProductByStatus('1')) / $pageSize);
        } else {
            $data['items'] = $this->product->searchFilter($search, $page, $pageSize, $fCategory, $fPrice);
            $data['maxPage'] = ceil(sizeof($this->product->searchFilter($search, 1, PHP_INT_MAX, $fCategory, $fPrice)) / $pageSize);
            $data['search'] = $search;
        }
        if (sizeof($data['items']) == 0) {
            $this->session->set_flashdata('message', '<div class="alert text-center" role="alert" style="color: red">Something you are looking for is not here!</div>');
        }

        foreach ($data['items'] as &$item) {
            $item['price'] = $this->rupiah($item['price']);
        }

        $this->load->view('shopping/shop', $data);

        $this->load->view('templates/footer');
    }

    public function details($id)
    {
        $this->load->view('templates/head');
        $this->load->view('templates/navbar');

        $this->load->model('Product_model');
        $data['desc'] = $this->Product_model->getProductById($id);
        $data['desc']['price'] = $this->rupiah($data['desc']['price']);

        $this->load->view('shopping/detail', $data);

        $this->load->view('templates/footer');
    }

    public function rupiah($price)
    {
        $hasil_rupiah = "Rp " . number_format($price, 2, ',', '.');
        return $hasil_rupiah;
    }
}
